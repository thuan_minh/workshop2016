class StaticPagesController < ApplicationController
	def home
	end

	def call_for_papers
	end

	def origanization
	end

	def program_committee
	end

	def program_overview
	end

	def paper_submission
	end

	def post_proceedings
	end

	def important_dates
	end

	def venue
	end

	def futher_information
	end

	def previous_workshops
	end

	def download_papers
	end

	def evaluation_campaign
	end
end
